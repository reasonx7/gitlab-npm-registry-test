export const info = (message: string) => {
  console.log(message);
};

export const error = (message: string) => {
  console.error(message);
};
