# @reasonx7/demo-logger

This is just a test package for GitLab NPM registry demo.

## Setup

### NPM Registry

Login into your registry. More details can be found on [GitLab documentation site](https://docs.gitlab.com/ee/user/packages/npm_registry/#publishing-a-package-via-the-command-line).

```bash
npm login --scope=reasonx7 --registry=https://gitlab.com/api/v4/projects/gitlab-npm-registry-test/packages/npm/
```

### Package Setup

Specify list of files & folders that should be published. Files that are not on the list will be ignored:

```json
{
  "name": "@reasonx7/demo-logger",
  "files": [
    "dist"
  ]
}
```

Specify `publishConfig`:

```json
{
  "name": "@reasonx7/demo-logger",
  "publishConfig": {
    "@reasonx7:registry": "https://gitlab.com/api/v4/projects/58293701/packages/npm/"
  }
}
```

This way we won't put `src` folder into the final package.

Specify `build` step on `prepublish` NPM hook:

```json
{
  "name": "@reasonx7/demo-logger",
  "scripts": {
    "build": "tsc",
    "prepublish": "npm run build"
  }
}
```

## Publish

Publish can be done using `npm publish` command ([link](https://docs.npmjs.com/cli/v8/commands/npm-publish))
or via open source release tools like `relese-it`.
